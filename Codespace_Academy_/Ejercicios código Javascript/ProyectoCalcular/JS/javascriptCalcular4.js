
var iva = [];
var valores=[];

function cantidades_iva(){
  iva=[];
  valores=[];
  var botonIva=document.getElementsByClassName("iva");
    for(i=0; i<botonIva.length; i++){
      iva.push(botonIva[i].value);
    }

  var cantidad=document.getElementsByClassName("valor");
    for(i=0; i<cantidad.length; i++){
      valores.push(cantidad[i].value);

    }
}

const evento_click = function(){

    genera_filas();
    this.removeEventListener("click", evento_click);
};


var botonCalcula1=document.getElementById("0");
var botonCalcula2=document.getElementById("1");
var botonCalcula3=document.getElementById("2");

document.getElementById("0").addEventListener("mouseover", mouseOver0);
function mouseOver0() {
  cantidades_iva();
  document.getElementById("total1").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
}

document.getElementById("1").addEventListener("mouseover", mouseOver1);
function mouseOver1() {
  cantidades_iva();
  document.getElementById("total2").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
}

document.getElementById("2").addEventListener("mouseover", mouseOver2);
function mouseOver2() {
  cantidades_iva();
  document.getElementById("total3").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
}

document.getElementsByClassName("calcula")[document.getElementsByClassName("calcula").length -1].addEventListener("click", evento_click);
document.getElementsByClassName("calcula")[document.getElementsByClassName("calcula").length -1].addEventListener("click", evento_click);




function iva_completo(num){
  var suma_iva=0;
  for(var i=0; i<iva.length; i++ ){
    suma_iva=suma_iva + Number(valores[i]) * Number(iva[i]) / 100;
  }
  return suma_iva;
  //return Number(document.getElementById("ivaFinal").innerHTML) + Number(valores[num]) * Number(iva[num]) / 100;
}

function subtotal_completo(num){
  var suma_subtotal=0;
  for(var i=0; i<valores.length; i++){
    suma_subtotal=suma_subtotal + Number(valores[i]);
  }
  return suma_subtotal;
  //return Number(document.getElementById("totalFinal").innerHTML) + Number(valores[num]);
}

function resultado_total(num){
  var suma_total=0;
  suma_total=iva_completo() + subtotal_completo();
  return suma_total;
  //return Number(document.getElementById("total").innerHTML) + (Number(valores[num]) * Number(iva[num]) / 100) + Number(valores[num]);
}

function genera_filas(){

  /* Creación de todos los elementos necesarios para crear la nueva línea. */
  var caja_linea = document.createElement("DIV");
  var input_valor = document.createElement("INPUT");
  var input_calcula = document.createElement("INPUT");
  var select_iva = document.createElement("SELECT");
  var option_10 = document.createElement("OPTION");
  var option_21 = document.createElement("OPTION");
  var crea_p = document.createElement("P");

  /* Vamos a inicializar los atributos de los elementos. */
  option_10.value = 10;
  option_10.innerText = "10%";
  option_21.value = 21;
  option_21.innerText = "21%";

  select_iva.appendChild(option_10);
  select_iva.appendChild(option_21);

  input_valor.className="valor";
  input_valor.placeholder="Escribe cantidad";
  input_valor.style="margin-right: 4px"
  input_calcula.className="calcula";
  input_calcula.placeholder="Calcula";
  input_calcula.id=" " + iva.length;
  input_calcula.addEventListener("click", evento_click);
  input_calcula.style="margin-left: 4px"

  crea_p.innerText = "Total: ";
  crea_p.className = "conIva";

  caja_linea.className = "boxCalcular";

  /* Añadimos los contenidos preparados al div que vamos a crear */
  caja_linea.appendChild(input_valor);
  caja_linea.appendChild(select_iva);
  caja_linea.appendChild(input_calcula);
  caja_linea.appendChild(crea_p);
  document.getElementById("cajas").appendChild(caja_linea);


  console.log("id del elemento", iva.length);

}
