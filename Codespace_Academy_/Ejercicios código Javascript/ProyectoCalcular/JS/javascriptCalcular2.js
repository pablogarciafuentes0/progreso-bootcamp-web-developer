var iva = [];
var valores=[];

function cantidades_iva(){
  iva=[];
  valores=[];
  var botonIva=document.getElementsByClassName("iva");
    for(i=0; i<botonIva.length; i++){
      iva.push(botonIva[i].value);
    }

  var cantidad=document.getElementsByClassName("valor");
    for(i=0; i<cantidad.length; i++){
      valores.push(cantidad[i].value);

    }
}


var botonCalcula1=document.getElementById("0");
var botonCalcula2=document.getElementById("1");
var botonCalcula3=document.getElementById("2");

botonCalcula1.addEventListener("click", function(){
  cantidades_iva();
  document.getElementById("total1").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
});

botonCalcula2.addEventListener("click", function(){
  cantidades_iva();
  document.getElementById("total2").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
});

botonCalcula3.addEventListener("click", function(){
  cantidades_iva();
  document.getElementById("total3").innerHTML=(Number(valores[this.id]) * Number(iva[this.id]) / 100) + Number(valores[this.id]);
  document.getElementById("totalFinal").innerHTML=subtotal_completo(this.id);
  document.getElementById("ivaFinal").innerHTML=iva_completo(this.id);
  document.getElementById("total").innerHTML=resultado_total(this.id);
});


function iva_completo(num){
  return Number(document.getElementById("ivaFinal").innerHTML) + Number(valores[num]) * Number(iva[num]) / 100;
}

function subtotal_completo(num){
  return Number(document.getElementById("totalFinal").innerHTML) + Number(valores[num]);
}

function resultado_total(num){
  return Number(document.getElementById("total").innerHTML) + (Number(valores[num]) * Number(iva[num]) / 100) + Number(valores[num]);
}
