EJERCICIO TIENDA VIRTUAL EN PHPMYADMIN.

--A) Mostrar de un comprador sus artículos y deseos.
(SELECT "compra", usuarios.usuario, productos.producto FROM `usuarios`
JOIN compras ON usuarios.id_usuario=compras.id_usuario
JOIN productos ON compras.id_producto=productos.id_producto
WHERE usuarios.id_usuario=1)
UNION
(SELECT "deseo", usuarios.usuario, productos.producto FROM `lista_deseos`
JOIN productos ON lista_deseos.id_deseo=productos.id_producto
JOIN usuarios ON lista_deseos.id_usuario=usuarios.id_usuario
WHERE lista_deseos.id_usuario=1);

--B) Obtener el artículo más vendido
SELECT productos.producto, SUM(compras.cantidad) AS mas_vendidos FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto
ORDER BY mas_vendidos DESC ;

--C) Dado un usuario, conocer su último pedido.
SELECT usuarios.usuario, compras.fecha_pedido AS "Último Pedido" FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
JOIN usuarios ON compras.id_usuario=usuarios.id_usuario
WHERE usuarios.id_usuario=1
GROUP BY compras.id_producto
ORDER BY compras.fecha_pedido DESC;

--D) Listar todos los artículos incluyendo una columna que indique cuantas veces está vendido y cuántas veces está en la lista de deseos.
(SELECT productos.producto, SUM(compras.cantidad) AS "Más Vendidos" FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto
ORDER BY SUM(compras.cantidad) DESC)
UNION
(SELECT productos.producto, 0 FROM productos
WHERE productos.id_producto NOT IN (SELECT productos.id_producto FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto));

--Seleccionar la lista de la compra al completo con todos los usuarios y el precio final para cada compra.
SELECT usuarios.usuario, direcciones.cod_postal, compras.fecha_pedido, productos.producto, compras.cantidad, productos.precio, compras.iva, (productos.precio + compras.iva) AS "PRECIO FINAL" FROM `compras`
JOIN usuarios ON compras.id_usuario=usuarios.id_usuario
JOIN productos ON compras.id_producto=productos.id_producto
JOIN direcciones ON compras.id_direccion=direcciones.cod_postal
;
