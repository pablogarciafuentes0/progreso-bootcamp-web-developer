EJERCICIO SEGUROS EN PHPMYADMIN.

--A) Obtener todas las pólizas de un asegurado.
Select * FROM polizas JOIN asegurados ON polizas.dni_asegurado=asegurados.dni_asegurado WHERE polizas.dni_asegurado='44966021D';

--B) Obtener las pólizas vencidas y no renovadas.
Select * FROM polizas WHERE polizas.fecha_fin < NOW();

--C) Obtener todos los siniestros por meses.
Select MONTHNAME(fecha_siniestro), num_poliza, estado_siniestro FROM siniestros;

--D) Obtener el histórico de un siniestro.
SELECT * FROM siniestros WHERE siniestros.num_poliza='A001';

--E) Obtener toda la cartera de un comercial, calculando el total de las pólizas generadas. Un comercial comisiona el 5% de la póliza.
SELECT carteras.cif, nombre, apellidos, carteras.num_poliza, polizas.precio, (polizas.precio * 0.05) AS ganancia_comision, (polizas.precio - (polizas.precio * 0.05)) AS total FROM carteras
JOIN comerciales ON carteras.cif=comerciales.cif 
JOIN polizas ON carteras.num_poliza=polizas.num_poliza
WHERE carteras.cif='2583'
GROUP BY num_poliza;

--F) Obtener el listado de los 10 clientes que menos siniestros han tenido.
SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, 0 as num_siniestros FROM asegurados 
WHERE dni_asegurado NOT IN(SELECT polizas.dni_asegurado FROM polizas JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza)

UNION

SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, COUNT(*) AS num_siniestros FROM  siniestros 
JOIN polizas ON siniestros.num_poliza=polizas.num_poliza
JOIN asegurados ON asegurados.dni_asegurado=polizas.dni_asegurado
GROUP BY polizas.dni_asegurado;

--G) Obtener el listado de los 10 clientes que más siniestros han tenido.
SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, COUNT(*) AS num_siniestros FROM  siniestros 
JOIN polizas ON siniestros.num_poliza=polizas.num_poliza
JOIN asegurados ON asegurados.dni_asegurado=polizas.dni_asegurado
GROUP BY polizas.dni_asegurado;

--H) Para renovar una póliza se aplica el siguiente criterio:
-- Si esa póliza no ha tenido un siniestro en un año: -7% del valor actual.
SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 0.80*polizas.precio as new_precio FROM asegurados
JOIN polizas ON asegurados.dni_asegurado=polizas.dni_asegurado
WHERE asegurados.dni_asegurado NOT IN 
(SELECT polizas.dni_asegurado FROM polizas 
JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza
WHERE YEAR(siniestros.fecha_siniestro) >= YEAR(NOW()-2))
        
UNION DISTINCT

SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 0.93*polizas.precio as new_precio FROM asegurados
JOIN polizas ON asegurados.dni_asegurado=polizas.dni_asegurado
WHERE asegurados.dni_asegurado NOT IN 
(SELECT polizas.dni_asegurado FROM polizas 
JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza
WHERE YEAR(siniestros.fecha_siniestro) >= YEAR(NOW()-1));

-- Si esa póliza no ha tenido un siniestro en dos años: -20% del valor actual.
SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 0.80*polizas.precio as new_precio FROM asegurados
JOIN polizas ON asegurados.dni_asegurado=polizas.dni_asegurado
WHERE asegurados.dni_asegurado 
NOT IN (SELECT polizas.dni_asegurado FROM polizas 
        JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza
        WHERE YEAR(siniestros.fecha_siniestro) >= YEAR(NOW()-2));

-- Si ha tenido más de un siniestro en un año: +15%.
SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 1.15*polizas.precio as new_precio FROM  siniestros 
JOIN polizas ON siniestros.num_poliza=polizas.num_poliza
JOIN asegurados ON asegurados.dni_asegurado=polizas.dni_asegurado
GROUP BY polizas.dni_asegurado, YEAR(siniestros.fecha_siniestro)

UNION DISTINCT

SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 0.80*polizas.precio as new_precio FROM asegurados
JOIN polizas ON asegurados.dni_asegurado=polizas.dni_asegurado
WHERE asegurados.dni_asegurado NOT IN 
(SELECT polizas.dni_asegurado FROM polizas 
JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza
WHERE YEAR(siniestros.fecha_siniestro) >= YEAR(NOW()-2))
        
UNION DISTINCT

SELECT asegurados.dni_asegurado, asegurados.nombre, asegurados.apellidos, polizas.num_poliza, 0.93*polizas.precio as new_precio FROM asegurados
JOIN polizas ON asegurados.dni_asegurado=polizas.dni_asegurado
WHERE asegurados.dni_asegurado NOT IN 
(SELECT polizas.dni_asegurado FROM polizas 
JOIN siniestros ON siniestros.num_poliza=polizas.num_poliza
WHERE YEAR(siniestros.fecha_siniestro) >= YEAR(NOW()-1));
--Generar la vista que permite calcular el precio de renovación de una póliza.




--Código creación de la Base de Datos 'Seguros'.

CREATE TABLE asegurados (
  dni_asegurado varchar(40) NOT NULL,
  nombre char(20) NOT NULL,
  apellidos varchar(40) NOT NULL,
  direccion varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE carteras (
  id int(11) NOT NULL,
  cif int(11) NOT NULL,
  num_poliza varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE comerciales (
  id int(11) NOT NULL,
  nombre char(20) NOT NULL,
  apellidos varchar(40) NOT NULL,
  cif int(11) NOT NULL,
  direccion varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE estadodelpago (
  id int(11) NOT NULL,
  estado_pago varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE estadodelsiniestro (
  id int(11) NOT NULL,
  estado_siniestro varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE formadepago (
  id int(11) NOT NULL,
  forma_pago varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE polizas (
  dni_asegurado varchar(40) NOT NULL,
  num_poliza varchar(40) NOT NULL,
  fecha_inicio date NOT NULL,
  fecha_fin date NOT NULL,
  precio int(11) NOT NULL,
  forma_pago varchar(40) NOT NULL,
  estado_pago varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE siniestros (
  id int(11) NOT NULL,
  num_poliza varchar(40) NOT NULL,
  fecha_siniestro date NOT NULL,
  estado_siniestro varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--Inserción de datos en las tablas creadas.
INSERT INTO asegurados (dni_asegurado, nombre, apellidos, direccion) VALUES
('44966021D', 'Pablo', 'Garcia Fuentes', 'Calle Setenil 8'),
('45199521L', 'Ana', 'Bastos Bayon', 'Calle Lopez Blanco 15'),
('43177485M', 'Leo', 'Bayon Fuentes', 'Calle Hungria 19'),
('41377436H', 'Daniel', 'Urbano De La Rua', 'Calle Desarrollador 69'),
('46199874K', 'Pedro', 'Perez Manzano', 'Calle Ancha 4');

INSERT INTO carteras (id, cif, num_poliza) VALUES
(1, 2583, 'A001'),
(2, 2583, 'A002'),
(3, 3421, 'A003'),
(4, 3421, 'A004'),
(5, 1234, 'A005');

INSERT INTO comerciales (id, nombre, apellidos, cif, direccion) VALUES
(1, 'Rosa', 'Martinez Laurel', 1234, 'Calle Larga 2'),
(2, 'Rafael', 'Ortiz Vega', 3421, 'Calle Alcazaba 12'),
(3, 'Elena', 'Rodriguez Martos', 2583, 'Avenida Corrales 8');

INSERT INTO estadodelpago (id, estado_pago) VALUES
(2, 'pagado'),
(1, 'pendiente'),
(3, 'rechazado');

INSERT INTO estadodelsiniestro (id, estado_siniestro) VALUES
(3, 'en proceso de reparacion'),
(4, 'finalizada'),
(1, 'pendiente de peritar'),
(2, 'peritacion rechazada');

INSERT INTO formadepago (id, forma_pago) VALUES
(3, 'domiciliacion bancaria'),
(2, 'tarjeta'),
(1, 'transferencia');

INSERT INTO polizas (dni_asegurado, num_poliza, fecha_inicio, fecha_fin, precio, forma_pago, estado_pago) VALUES
('', 'A001', '2020-01-01', '2020-02-01', 100, 'domiciliacion bancaria', 'pagado'),
('', 'A002', '2020-01-02', '2020-02-02', 100, 'tarjeta', 'pendiente'),
('', 'A003', '2020-01-03', '2020-03-03', 200, 'transferencia', 'rechazado'),
('', 'A004', '2020-01-04', '2020-03-04', 200, 'domiciliacion bancaria', 'pendiente'),
('', 'A005', '2020-01-05', '2020-02-05', 100, 'tarjeta', 'pagado');

INSERT INTO siniestros (id, num_poliza, fecha_siniestro, estado_siniestro) VALUES
(5, 'A001', '2020-01-10', 'en proceso de reparacion'),
(6, 'A002', '2020-01-12', 'finalizada'),
(7, 'A003', '2020-01-13', 'pendiente de peritar'),
(8, 'A004', '2020-01-23', 'peritacion rechazada'),
(9, 'A005', '2020-01-30', 'pendiente de peritar'),
(10, 'A001', '2020-01-28', 'finalizada'),
(11, 'A003', '2020-01-31', 'peritacion rechazada');

