-- VIDEOCLUB

--A) Peliculas (titulo, año, categoria, duracion, nacionalidad, director, puntuacion)
--B) Socios (dni, nombre, apellidos, fecha nacimiento)

--Necesito gestionar qué usuarios tienen alquilada alguna de las películas.

--Cómo puedo conocer:
--A) La lista de socios que han alquilado películas y los datos de éstas.
--B) Todas las películas y los nombres de los socios que han alquilado películas.
--C) Listado de películas alquiladas y no alquiladas (todo en un listado).
--D) Listado de películas que llevan alquiladas más de 2 días. (Sin poner la fecha a fuego).
--E) Cambiar los NULL mostrados por un valor en lugar de NULL.

CREATE DATABASE IF NOT EXISTS videoclub;

CREATE TABLE IF NOT EXISTS peliculas(
  id_pelicula INT NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(50) NOT NULL,
  año INT NOT NULL,
  categoria CHAR(20) NOT NULL,
  duracion CHAR(20) NOT NULL,
  nacionalidad CHAR (20) NOT NULL,
  director CHAR(20) NOT NULL,
  puntuacion TINYINT NOT NULL,
  PRIMARY KEY (id_pelicula)
);

CREATE TABLE IF NOT EXISTS socios (
  dni CHAR(20) NOT NULL,
  nombre CHAR (20) NOT NULL,
  apellido VARCHAR (50),
  birth_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS alquileres (
  dni CHAR(20) NOT NULL,
  id_pelicula TINYINT NOT NULL,
  from_date DATE NOT NULL
);

INSERT INTO peliculas (titulo, año, categoria, duracion, nacionalidad, director, puntuacion) VALUES (
  'Jurassic Park', 1993, 'Aventura', '2h 7min', 'EEUU', 'Steven Spielberg', 8
), (
  'Star Wars', 1977, 'Acción', '2h 1min', 'EEUU', 'George Lucas', 9
), (
  'Ocho Apellidos Vascos', 2014, 'Comedia', '1h 38min', 'España', 'Emilio Martínez', 6
), (
  'La vida es bella', 1997, 'Drama', '1h 56min', 'Italia', 'Roberto Benigni', 9
), (
  'La Lista de Schindler', 1993, 'Drama', '3h 15min', 'EEUU', 'Steven Spielberg', 9
), (
  'El Señor de los Anillos', 2001, 'Aventura', '2h 58min', 'EEUU', 'Peter Jackson', 9
), (
  'Martin (Hache)', 1997, 'Drama', '2h 3min', 'Argentina', 'Adolfo Aristarain', 7
), (
  'Toy Story', 1995, 'Animación', '1h 21min', 'EEUU', 'John Lasseter', 8
), (
  'Memento', 2000, 'Thriller', '1h 53min', 'EEUU', 'Christopher Nolan', 8
), (
  'El Padrino', 1972, 'Crimen', '2h 55min', 'EEUU', 'Francis Ford Coppola', 9
);

INSERT INTO socios VALUES (
  '44966021D', 'Pablo', 'Garcia', '1986-06-20'
), (
  '15475648P', 'Daniel', 'Bombastik', '1984-06-12'
), (
  '22844075F', 'Pedro', 'Martínez', '1989-08-11'
), (
  '32811546K', 'Manuel', 'Fernández', '1988-12-03'
), (
  '11523147L', 'Francisco', 'Gutiérrez', '1999-05-14'
), (
  '48199548S', 'Jose Antonio', 'Flores', '1987-02-20'
), (
  '41955123D', 'Luis', 'Manzano', '1998-04-20'
), (
  '52632816M', 'Ana', 'Bastos', '1991-05-06'
);

INSERT INTO alquileres VALUES (
  '44966021D', 2, '2020-01-10'
), (
  '15475648P', 8, '2020-01-13'
), (
  '52632816M', 5, '2020-01-11'
), (
  '48199548S', 7, '2020-01-14'
), (
  '48199548S', 1, '2020-01-08'
);


--Listado de todas las películas, alquiladas y no alquiladas (todo en un 1 listado)
-- SELECT * FROM peliculas LEFT JOIN alquileres ON peliculas.id_pelicula=alquileres.id_pelicula;

--Todas las películas y los nombres de los socios que han sido alquiladas.
--SELECT *, socios.nombre, socios.dni FROM peliculas LEFT JOIN alquileres ON alquileres.id_pelicula=peliculas.id_pelicula LEFT JOIN socios ON socios.dni=alquileres.dni;

--Listado de socios que han alquilado películas y los datos de éstas.
--SELECT * FROM alquileres JOIN socios ON alquileres.dni=socios.dni JOIN peliculas ON alquileres.id_pelicula=peliculas.id_pelicula;

--Listado de películas que llevan alquiladas más de 2 días. (Sin poner la fecha a fuego).
--SELECT * FROM alquileres WHERE DATEDIFF(NOW(), from_date) > 2;

--Cambiar los NULL mostrados por un valor en lugar de NULL.
--SELECT *, IFNULL(alquileres.id_pelicula, 'En Alquiler') AS 'Estado' FROM peliculas LEFT JOIN alquileres ON peliculas.id_pelicula=alquileres.id_pelicula;
