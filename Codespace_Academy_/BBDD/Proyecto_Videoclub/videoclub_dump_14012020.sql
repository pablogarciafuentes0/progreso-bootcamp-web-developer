-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: videoclub
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alquileres`
--

DROP TABLE IF EXISTS `alquileres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alquileres` (
  `dni` char(20) NOT NULL,
  `id_pelicula` tinyint(4) NOT NULL,
  `from_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alquileres`
--

LOCK TABLES `alquileres` WRITE;
/*!40000 ALTER TABLE `alquileres` DISABLE KEYS */;
INSERT INTO `alquileres` VALUES ('44966021D',2,'2020-01-10'),('15475648P',8,'2020-01-13'),('52632816M',5,'2020-01-11'),('48199548S',7,'2020-01-14'),('48199548S',1,'2020-01-08');
/*!40000 ALTER TABLE `alquileres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peliculas`
--

DROP TABLE IF EXISTS `peliculas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peliculas` (
  `id_pelicula` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `año` int(11) NOT NULL,
  `categoria` char(20) NOT NULL,
  `duracion` char(20) NOT NULL,
  `nacionalidad` char(20) NOT NULL,
  `director` char(20) NOT NULL,
  `puntuacion` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_pelicula`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peliculas`
--

LOCK TABLES `peliculas` WRITE;
/*!40000 ALTER TABLE `peliculas` DISABLE KEYS */;
INSERT INTO `peliculas` VALUES (1,'Jurassic Park',1993,'Aventura','2h 7min','EEUU','Steven Spielberg',8),(2,'Star Wars',1977,'Acción','2h 1min','EEUU','George Lucas',9),(3,'Ocho Apellidos Vascos',2014,'Comedia','1h 38min','España','Emilio Martínez',6),(4,'La vida es bella',1997,'Drama','1h 56min','Italia','Roberto Benigni',9),(5,'La Lista de Schindler',1993,'Drama','3h 15min','EEUU','Steven Spielberg',9),(6,'El Señor de los Anillos',2001,'Aventura','2h 58min','EEUU','Peter Jackson',9),(7,'Martin (Hache)',1997,'Drama','2h 3min','Argentina','Adolfo Aristarain',7),(8,'Toy Story',1995,'Animación','1h 21min','EEUU','John Lasseter',8),(9,'Memento',2000,'Thriller','1h 53min','EEUU','Christopher Nolan',8),(10,'El Padrino',1972,'Crimen','2h 55min','EEUU','Francis Ford Coppola',9);
/*!40000 ALTER TABLE `peliculas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socios`
--

DROP TABLE IF EXISTS `socios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socios` (
  `dni` char(20) NOT NULL,
  `nombre` char(20) NOT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socios`
--

LOCK TABLES `socios` WRITE;
/*!40000 ALTER TABLE `socios` DISABLE KEYS */;
INSERT INTO `socios` VALUES ('44966021D','Pablo','Garcia','1986-06-20'),('15475648P','Daniel','Bombastik','1984-06-12'),('22844075F','Pedro','Martínez','1989-08-11'),('32811546K','Manuel','Fernández','1988-12-03'),('11523147L','Francisco','Gutiérrez','1999-05-14'),('48199548S','Jose Antonio','Flores','1987-02-20'),('41955123D','Luis','Manzano','1998-04-20'),('52632816M','Ana','Bastos','1991-05-06');
/*!40000 ALTER TABLE `socios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-14 17:21:18
