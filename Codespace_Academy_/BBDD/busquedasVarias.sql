



--Seleccionar el máximo año de contratación de la Database employees.
SELECT MAX(YEAR(hire_date)) FROM employees;

--Seleccionar el mínimo año de contratación de la Database employees.
SELECT MIN(YEAR(hire_date)) FROM employees;

--Añadir 17 años a la fecha de contratación.
UPDATE employees SET hire_date=DATE_ADD(hire_date, INTERVAL 17 YEAR)
--Ahora cuando hagamos esto:
SELECT MAX(YEAR(hire_date)) FROM employees;
--Nos dará el resultado anterior con 17 años más.




SELECT emp_no, title, DATE_FORMAT(from_date, '%d/%m/%Y')
AS from_date_ES, DATE_FORMAT(DATE_ADD(from_date, INTERVAL 10 YEAR), '%d/%m/%Y')
AS 'Fecha de Celebracion', DATEDIFF(to_date, from_date)
AS 'Resta de fechas' FROM titles
WHERE from_date>=DATE_SUB(NOW(), INTERVAL 2 YEAR) LIMIT 20;



SELECT emp_no, first_name, DATE_FORMAT(hire_date, '%d/%m/%Y')
AS hire_date_ES, DATE_FORMAT(DATE_ADD(hire_date, INTERVAL 10 YEAR), '%d/%m/%Y')
AS 'Fecha de Celebracion' FROM employees
WHERE hire_date>=DATE_SUB(NOW(), INTERVAL 2 YEAR) LIMIT 2;

--Crear la VIEW que llamaremos empleadosMenos5años.
CREATE VIEW empleadosMenos5años AS SELECT emp_no, first_name, DATE_FORMAT(hire_date, '%d/%m/%Y') AS from_date_ES, DATE_FORMAT(DATE_ADD(hire_date, INTERVAL 10 YEAR), '%d/%m/%Y') AS 'Fecha de Celebracion' FROM employees WHERE hire_date>=DATE_SUB(NOW(), INTERVAL 2 YEAR) LIMIT 10;

--Para ver el detalle de la VIEW creada.
SHOW CREATE VIEW empleadosMenos5años;

--Modificar la VIEW creada.
ALTER VIEW empleadosMenos5años AS SELECT emp_no, first_name, DATE_FORMAT(hire_date, '%d/%m/%Y') AS from_date_ES, DATE_FORMAT(DATE_ADD(hire_date, INTERVAL 10 YEAR), '%d/%m/%Y') AS 'Fecha de Celebracion' FROM employees WHERE hire_date>=DATE_SUB(NOW(), INTERVAL 2 YEAR) LIMIT 10;


--Crear una tabla nueva.
CREATE TABLE employees_copia (
  emp_no INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  birth_date DATE NOT NULL,
  first_name VARCHAR(14) NOT NULL,
  last_name VARCHAR(16) NOT NULL,
  gender ENUM('M', 'F') NOT NULL,
  hire_date DATE NOT NULL);

--Introducir valores a la tabla.
INSERT INTO employees_copia (
  birth_date,
  first_name,
  last_name,
  hire_date,
  gender
) VALUES ('1982-10-10', 'Leo', 'Messi', '2020-01-01', 'M'),
         ('1980-10-10', 'Luis', 'Suarez', '2020-01-01', 'M'),
         ('1970-10-10', 'Victor', 'Sanchez del Amo', '2020-01-01', 'M');


SELECT emp_no, title FROM titles UNION DISTINCT SELECT emp_no, first_name FROM employees_copia;

--Seleccionar todo de compras y JOIN con usuarios.
SELECT * FROM `compras` JOIN usuarios ON compras.id_usuario=usuarios.id_usuario;

--Cambiar el nombre a una columna.
ALTER TABLE employees_copia CHANGE COLUMN first_date first_name VARCHAR(14) NOT NULL;

--Levantar el servidor de phpMyAdmin desde consola.
--C:\Users\A101\Downloads\phpMyAdmin\phpMyAdmin-4.9.4-all-languages>C:\Users\A101\Downloads\php\php -S localhost:8081

--Mostrar las compras de un usuario en particular.
SELECT usuarios.usuario, compras.id_direccion, productos.producto, productos.precio, compras.iva, compras.total FROM `usuarios`
JOIN compras ON usuarios.id_usuario=compras.id_usuario
JOIN productos ON compras.id_producto=productos.id_producto
WHERE usuarios.id_usuario=1;

--Mostrar los articulos deseados por un usuario en particular.
SELECT usuarios.usuario, productos.producto FROM `lista_deseos`
JOIN productos ON lista_deseos.id_deseo=productos.id_producto
JOIN usuarios ON lista_deseos.id_usuario=usuarios.id_usuario
WHERE lista_deseos.id_usuario=1;

EJERCICIO TIENDA VIRTUAL EN PHPMYADMIN.

--A) Mostrar de un comprador sus artículos y deseos.
(SELECT "compra", usuarios.usuario, productos.producto FROM `usuarios`
JOIN compras ON usuarios.id_usuario=compras.id_usuario
JOIN productos ON compras.id_producto=productos.id_producto
WHERE usuarios.id_usuario=1)
UNION
(SELECT "deseo", usuarios.usuario, productos.producto FROM `lista_deseos`
JOIN productos ON lista_deseos.id_deseo=productos.id_producto
JOIN usuarios ON lista_deseos.id_usuario=usuarios.id_usuario
WHERE lista_deseos.id_usuario=1);

--B) Obtener el artículo más vendido
SELECT productos.producto, SUM(compras.cantidad) AS "Más Vendidos" FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto
ORDER BY SUM(compras.cantidad) DESC ;

--C) Dado un usuario, conocer su último pedido.
SELECT usuarios.usuario, compras.fecha_pedido AS "Último Pedido" FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
JOIN usuarios ON compras.id_usuario=usuarios.id_usuario
WHERE usuarios.id_usuario=1
GROUP BY compras.id_producto
ORDER BY compras.fecha_pedido DESC;

--D) Listar todos los artículos incluyendo una columna que indique cuantas veces está vendido y cuántas veces está en la lista de deseos.
(SELECT productos.producto, SUM(compras.cantidad) AS "Más Vendidos" FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto
ORDER BY SUM(compras.cantidad) DESC)
UNION
(SELECT productos.producto, 0 FROM productos
WHERE productos.id_producto NOT IN (SELECT productos.id_producto FROM `compras`
JOIN productos ON compras.id_producto=productos.id_producto
GROUP BY compras.id_producto));

--Seleccionar la lista de la compra al completo con todos los usuarios y el precio final para cada compra.
SELECT usuarios.usuario, direcciones.cod_postal, compras.fecha_pedido, productos.producto, compras.cantidad, productos.precio, compras.iva, (productos.precio + compras.iva) AS "PRECIO FINAL" FROM `compras`
JOIN usuarios ON compras.id_usuario=usuarios.id_usuario
JOIN productos ON compras.id_producto=productos.id_producto
JOIN direcciones ON compras.id_direccion=direcciones.cod_postal
;
