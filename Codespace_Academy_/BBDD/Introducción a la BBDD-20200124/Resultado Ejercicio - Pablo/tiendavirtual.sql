-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-01-2020 a las 14:56:24
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendavirtual`
--

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `apartado_a`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `apartado_a` (
`compra` varchar(6)
,`usuario` varchar(40)
,`producto` varchar(40)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `apartado_b`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `apartado_b` (
`producto` varchar(40)
,`Más Vendidos` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `apartado_c`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `apartado_c` (
`usuario` varchar(40)
,`Último Pedido` date
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `apartado_d`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `apartado_d` (
`producto` varchar(40)
,`Más Vendidos` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `descripcion` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `titulo`, `descripcion`) VALUES
(1, 'equipamiento', 'ropa para circular en moto'),
(2, 'accesorios', 'piezas adicionales para personalizar tu moto'),
(3, 'motos', 'dispositivos de disfrute máximo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compras` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_direccion` int(11) NOT NULL,
  `fecha_pedido` date NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `iva` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id_compras`, `id_usuario`, `id_direccion`, `fecha_pedido`, `id_producto`, `cantidad`, `subtotal`, `iva`, `total`) VALUES
(2, 2, 12307, '2020-01-14', 10, 1, 70, 14, 84),
(3, 5, 30040, '2020-01-10', 4, 1, 350, 73, 423),
(4, 3, 28040, '2020-01-12', 6, 1, 7500, 1575, 9075),
(5, 6, 41007, '2020-01-07', 7, 1, 50, 10, 60),
(6, 4, 29010, '2020-01-01', 3, 1, 8000, 1680, 9680),
(7, 1, 11510, '2020-01-03', 5, 1, 500, 105, 605),
(8, 1, 11007, '2020-01-04', 8, 1, 30, 6, 36),
(9, 4, 29007, '2020-01-08', 10, 1, 70, 14, 84);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id_direccion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cod_postal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id_direccion`, `id_usuario`, `cod_postal`) VALUES
(1, 1, 11510),
(2, 1, 11007),
(3, 1, 11040),
(4, 2, 29010),
(5, 2, 29007),
(6, 2, 29040),
(7, 3, 12310),
(8, 3, 12307),
(9, 3, 12340),
(10, 4, 28510),
(11, 4, 28007),
(12, 4, 28040),
(13, 5, 30510),
(14, 5, 30007),
(15, 5, 30040),
(16, 6, 41510),
(17, 6, 41007),
(18, 6, 41040);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_deseos`
--

CREATE TABLE `lista_deseos` (
  `id_lista_deseos` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_deseo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lista_deseos`
--

INSERT INTO `lista_deseos` (`id_lista_deseos`, `id_usuario`, `id_deseo`) VALUES
(11, 1, 4),
(12, 3, 10),
(13, 5, 5),
(14, 4, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `producto` varchar(40) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `id_categoria`, `producto`, `precio`) VALUES
(1, 1, 'Chaqueta', 200),
(2, 2, 'Cúpula', 40),
(3, 3, 'Honda CB650R', 8000),
(4, 1, 'Casco', 350),
(5, 2, 'Escape', 500),
(6, 3, 'Kawasaki Z900', 7500),
(7, 1, 'Guantes', 50),
(8, 2, 'Topes Anticaída', 30),
(9, 3, 'Suzuki GSX1000', 8500),
(10, 1, 'Botas', 70),
(11, 2, 'Puños calefactables', 45),
(12, 3, 'Ducati Monster 890', 9000),
(13, 1, 'Pantalones', 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(40) NOT NULL,
  `usuario` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`) VALUES
(2, 'BobMarley'),
(5, 'DarthVader'),
(3, 'JohnLennon'),
(6, 'LuckyLuke'),
(1, 'Pablo'),
(4, 'Yoda');

-- --------------------------------------------------------

--
-- Estructura para la vista `apartado_a`
--
DROP TABLE IF EXISTS `apartado_a`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `apartado_a`  AS  (select 'compra' AS `compra`,`usuarios`.`usuario` AS `usuario`,`productos`.`producto` AS `producto` from ((`usuarios` join `compras` on(`usuarios`.`id_usuario` = `compras`.`id_usuario`)) join `productos` on(`compras`.`id_producto` = `productos`.`id_producto`)) where `usuarios`.`id_usuario` = 1) union (select 'deseo' AS `deseo`,`usuarios`.`usuario` AS `usuario`,`productos`.`producto` AS `producto` from ((`lista_deseos` join `productos` on(`lista_deseos`.`id_deseo` = `productos`.`id_producto`)) join `usuarios` on(`lista_deseos`.`id_usuario` = `usuarios`.`id_usuario`)) where `lista_deseos`.`id_usuario` = 1) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `apartado_b`
--
DROP TABLE IF EXISTS `apartado_b`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `apartado_b`  AS  select `productos`.`producto` AS `producto`,sum(`compras`.`cantidad`) AS `Más Vendidos` from (`compras` join `productos` on(`compras`.`id_producto` = `productos`.`id_producto`)) group by `compras`.`id_producto` order by sum(`compras`.`cantidad`) desc ;

-- --------------------------------------------------------

--
-- Estructura para la vista `apartado_c`
--
DROP TABLE IF EXISTS `apartado_c`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `apartado_c`  AS  select `usuarios`.`usuario` AS `usuario`,`compras`.`fecha_pedido` AS `Último Pedido` from ((`compras` join `productos` on(`compras`.`id_producto` = `productos`.`id_producto`)) join `usuarios` on(`compras`.`id_usuario` = `usuarios`.`id_usuario`)) where `usuarios`.`id_usuario` = 1 group by `compras`.`id_producto` order by `compras`.`fecha_pedido` desc ;

-- --------------------------------------------------------

--
-- Estructura para la vista `apartado_d`
--
DROP TABLE IF EXISTS `apartado_d`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `apartado_d`  AS  (select `productos`.`producto` AS `producto`,sum(`compras`.`cantidad`) AS `Más Vendidos` from (`compras` join `productos` on(`compras`.`id_producto` = `productos`.`id_producto`)) group by `compras`.`id_producto`) union (select `productos`.`producto` AS `producto`,0 AS `0` from `productos` where !(`productos`.`id_producto` in (select `productos`.`id_producto` from (`compras` join `productos` on(`compras`.`id_producto` = `productos`.`id_producto`)) group by `compras`.`id_producto`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compras`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_direccion` (`id_direccion`),
  ADD KEY `Compras_Producto` (`id_producto`),
  ADD KEY `subtotal` (`subtotal`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id_direccion`),
  ADD KEY `id` (`id_usuario`),
  ADD KEY `id_2` (`id_usuario`),
  ADD KEY `id_3` (`id_usuario`),
  ADD KEY `cod_postal` (`cod_postal`);

--
-- Indices de la tabla `lista_deseos`
--
ALTER TABLE `lista_deseos`
  ADD PRIMARY KEY (`id_lista_deseos`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_producto` (`id_deseo`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `producto` (`producto`),
  ADD KEY `precio` (`precio`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id` (`id_usuario`),
  ADD KEY `usuario` (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compras` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id_direccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `lista_deseos`
--
ALTER TABLE `lista_deseos`
  MODIFY `id_lista_deseos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `Categorias-Productos` FOREIGN KEY (`id_categoria`) REFERENCES `productos` (`id_categoria`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `Compras-Direcciones` FOREIGN KEY (`id_direccion`) REFERENCES `direcciones` (`cod_postal`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `Compras-Subtotal` FOREIGN KEY (`subtotal`) REFERENCES `productos` (`precio`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `Compras-Usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `Compras_Producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD CONSTRAINT `Direcciones-Usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `lista_deseos`
--
ALTER TABLE `lista_deseos`
  ADD CONSTRAINT `Deseos-Producto` FOREIGN KEY (`id_deseo`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `Deseos-Usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
