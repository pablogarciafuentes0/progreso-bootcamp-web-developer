/*Esta es la forma de obtener elementos del DOM con JavaScript.*/
  var elementoA=document.getElementById("textoNew");
  console.log(elementoA);

  var todosLosP=document.getElementsByTagName("p");
  console.log(todosLosP);

  var todosLosObjetosDeClaseBWD6=document.getElementsByClassName("articulo bwd6");
  console.log(todosLosObjetosDeClaseBWD6);
/*Con esta instrucción se cambia el contenido de elementoA=AAAAAAA por "Manuel Atento".*/
  elementoA.innerHTML="Manuel Atento";

/*Esto sirve para crear un elemento nuevo e insertarlo en alguna parte del código,
en este caso lo insertamos al final del <Div> "principal".*/
  var newElementA=document.createElement("a");
  newElementA.href="https://www.google.com";
  newElementA.target="_blank";
  newElementA.style.color="red";
  newElementA.style.border="5px solid green";
  newElementA.style.padding="30px";
  newElementA.innerHTML="Este es el nuevo enlace creado de la nada";
  console.log(newElementA);
  document.getElementById("principal").appendChild(newElementA);

/*Añadir un meta en la cabecera.*/
  var miNuevoMeta=document.createElement("meta");
  //miNuevoMeta.content="text/html; charset=iso-8859-1";
  miNuevoMeta.setAttribute("charset", "iso-8859-1");
  console.log(miNuevoMeta);
  document.getElementsByTagName("head")[0].appendChild(miNuevoMeta);

/*Funcion para que nos salte un mensaje cuando hagamos click en determinadas zonas.catch
En este caso a través de HTML, está definido en <H2> Lorem ipsum...*/
  function myAlert(){
    alert("Ups, me han hecho click");
  }
/*En este caso a través de JS en la imagen del Div de arriba*/
  function myAlertImagen(){
    alert("Click en el Div de la imagen");
  }
  document.getElementById("imagenClick").onclick=function(){
  myAlertImagen();
  }
/*Aquí al pasar el ratón por encima de la imagen se pondrá el background en rojo.*/
  document.getElementById("imagenClick").onmouseover=function(){
  this.style.background="red";
  }

  /*Aquí si clickamos en los elementos con clase "imagenes" nos saltará el mensaje
  de la función myAlertImagen. */
  var elementoClaseImagen=document.getElementsByClassName("imagenes");
  for(var i=0; i<elementoClaseImagen.length; i++){
    elementoClaseImagen[i].onclick=function(){
      myAlertImagen();
    }
  }

/*Pasamos el ratón por encima de las <p> y cambian a color rojo.*/

  function cambiarColor(elemento){
    elemento.style.color="red";
  }

  function cogerTodasLasP(){
    var elementosP=document.getElementsByTagName("p");
    for(var i=0; i < elementosP.length; i++){
      elementosP[i].onmouseover=function(){
        cambiarColor(this);
      }
    }
  }

  cogerTodasLasP();

  /*Lo mismo de arriba, pero hecho por el profesor de forma básica.
  Aunque lo correcto es lo de arriba.

  var elementosP=document.getElementsByTagName("p");
  for(var i=0; i < elementosP.length; i++){
    elementosP[i].onmouseover=function(){
      this.style.color="red";
    }
    */

var h2EventListener=document.getElementById("h2EventListener");
h2EventListener.addEventListener("click", function(){
this.style.fontSize="50px";
  });

h2EventListener.addEventListener("click", function(){
this.style.color="green";
  });

h2EventListener.addEventListener("click", myAlert);
h2EventListener.addEventListener("click", myAlertImagen);
h2EventListener.removeEventListener("click", myAlert);
h2EventListener.removeEventListener("click", myAlertImagen);

/*Modificar una lista que tengamos.*/
var ModificarLista=document.getElementById("listaDePrueba");
ModificarLista.childNodes[1].style.color="purple";
ModificarLista.childNodes[1].style.fontWeight="bold";

/*Modificar el index 3 sin cambiar el indice del array.*/
ModificarLista.childNodes[1].nextSibling.style.color="green";

/*Añadir un elemento de la lista en la posición 3.*/
var nuevoElementoLista=document.createElement("li");
nuevoElementoLista.innerHTML="Elemento nuevo insertado con before";
ModificarLista.insertBefore(nuevoElementoLista, ModificarLista.childNodes[2]);
