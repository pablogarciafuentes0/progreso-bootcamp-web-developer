/***********  LÓGICA DE NEGOCIO ***********/



function ObtenerProducto() {

    var lista = CargaProducto();
    var lista_elementos = $("#lista");

    lista.forEach(element => {

        /*********  ELEMENTOS *******/
        var imagen = document.createElement('img');
        imagen.src = element.img;
        imagen.alt = element.alt;
        imagen.onclick = function (e) {
            //window.location.href = "./DetalleProducto.html?detail=" + element.id;
            window.open("./DetalleProducto.html?detail=" + element.id, "nombrePop-Up", "width=1450, height=730, top=85,left=200");
        };

        var descripcion = document.createElement('p');
        descripcion.innerText = element.desc;

        var precio = document.createElement('p');
        precio.innerText = element.precio;

        // aquí va la creación del ranking
        var ranking = CrearRanking(element.rank);
        //ranking = element.rank;

        var div_imagen = document.createElement('div');
        div_imagen.classList.add("imagen");
        div_imagen.append(imagen);

        var div_descripcion = document.createElement('div');
        div_descripcion.classList.add("descripcion");
        div_descripcion.append(descripcion);

        var div_precio = document.createElement('div');
        div_precio.classList.add("precio");
        div_precio.append(precio);

        var div_ranking = document.createElement('div');
        div_ranking.classList.add("valoracion");
        div_ranking.append(ranking);

        var li = document.createElement('li');
        li.className = "col-4 details";

        /*********** INSERTAR ELEMENTOS EN EL LI *************/
        li.append(div_imagen);
        li.append(div_descripcion);
        li.append(div_precio);
        li.append(div_ranking);

        lista_elementos.append(li);

    });




}

/*CREACIÓN RANKING DE LOS PRODUCTOS*/

function CrearRanking(rank) {
    console.log(rank);
    var ranking = document.createElement("p");
    for (let index = 0; index < 5; index++) {
        let checked = "";
        if (rank > index) {
            checked = " checked";
        }
        var span = document.createElement('span');
        span.className += " fa fa-star" + checked; //Así se añaden múltiples clases a un elemento, si usáramos ranking.classList.add(""); solo podríamos añadir 1.

        ranking.append(span);
    }
    return ranking;

}


