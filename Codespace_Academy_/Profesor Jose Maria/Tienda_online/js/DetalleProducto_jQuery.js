/*DETALLE DE PRODUCTO*/

const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('detail');
console.log(myParam);

/***********  LÓGICA DE NEGOCIO ***********/

function CrearDetalleProducto() {

    var lista = CargaProducto();
    var lista_elementos = $("#lista");

    $.each(lista, function (indexInArray, element) {

        if (element.id == myParam) {

            /*********  ELEMENTOS *******/

            var imagen = $("<img>", {
                "src": element.img,
                "alt": element.alt
            }).click(function () {
                window.open("./DetalleProducto.html?detail=" + element.id, "nombrePop-Up", "width=1450, height=730, top=85,left=200");
            });

            var descripcion = $("<p>").text(element.desc);

            var precio = $("<p>").text(element.precio);

            // aquí va la creación del ranking
            var ranking = CrearRanking(element.rank);

            var basket = $("<button>").text("Añadir a Cesta");

            /*********** CONTENEDORES DE LOS ELEMENTOS ************/

            var div_imagen = $("<div>", {
                "class": "imagen"
            }).append(imagen);

            var div_descripcion = $("<div>", {
                "class": "descripcion"
            }).append(descripcion);

            var div_precio = $("<div>", {
                "class": "precio"
            }).append(precio);

            var div_ranking = $("<div>", {
                "class": "valoracion"
            }).append(ranking);

            var div_cesta = $("<div>", {
                "class": "BotonCesta"
            }).append(basket);

            var li = $("<li>", {
                "class": "col-8 details"
            });

            /*********** INSERTAR ELEMENTOS EN EL LI *************/
            $(li).append(div_imagen);
            $(li).append(div_descripcion);
            $(li).append(div_precio);
            $(li).append(div_ranking);
            $(li).append(div_cesta);

            $(lista_elementos).append(li);

        } else {
            return;
        }


    });



}

