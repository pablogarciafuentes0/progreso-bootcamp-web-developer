/*DETALLE DE PRODUCTO*/

const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('detail');
console.log(myParam);


function CrearDetalleProducto() {

    var lista = CargaProducto();
    var lista_elementos = $("#lista");

    lista.forEach(element => {

        if (element.id == myParam) {

            /*********  ELEMENTOS *******/
            var imagen = document.createElement('img');
            imagen.src = element.img;
            imagen.alt = element.alt;
            imagen.onclick = function (e) {
                window.location.href = "./DetalleProducto.html?detail=" + element.id;
            };

            var descripcion = document.createElement('p');
            descripcion.innerText = element.desc;

            var precio = document.createElement('p');
            precio.innerText = element.precio;

            // aquí va la creación del ranking
            var ranking = CrearRanking(element.rank);
            //ranking = element.rank;

            var basket = document.createElement('button');
            basket.innerText = "Añadir a Cesta";


            var div_imagen = document.createElement('div');
            div_imagen.classList.add("imagen");
            div_imagen.append(imagen);

            var div_descripcion = document.createElement('div');
            div_descripcion.classList.add("descripcion");
            div_descripcion.append(descripcion);

            var div_precio = document.createElement('div');
            div_precio.classList.add("precio");
            div_precio.append(precio);

            var div_ranking = document.createElement('div');
            div_ranking.classList.add("valoracion");
            div_ranking.append(ranking);

            var div_cesta = document.createElement('div');
            div_cesta.classList.add("BotonCesta");
            div_cesta.append(basket);

            var li = document.createElement('li');
            li.className = "col-8 details";

            /*********** INSERTAR ELEMENTOS EN EL LI *************/
            li.append(div_imagen);
            li.append(div_descripcion);
            li.append(div_precio);
            li.append(div_ranking);
            li.append(div_cesta);

            lista_elementos.append(li);

        } else {
            return;
        }

    });





}

