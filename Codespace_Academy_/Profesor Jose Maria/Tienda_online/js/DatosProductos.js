/*************** DATOS ****************/

function CargaProducto() {
    var list = [];

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 0;
    item.img = "./img/casco1.jpg";
    item.alt = "Casco";
    item.desc = "Casco integral homologado para su uso en carretera.";
    item.precio = 200;
    item.rank = 5;
    list.push(item);
    // console.log(list[0].desc);

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 1;
    item.img = "./img/chaqueta.jpg";
    item.alt = "Chaqueta";
    item.desc = "Chaqueta de invierno con protecciones incorporadas.";
    item.precio = 175;
    item.rank = 4;
    list.push(item);
    //console.log(list[1].desc);

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 2;
    item.img = "./img/guantes.jpg";
    item.alt = "Guantes";
    item.desc = "Guantes de verano perforados para favorecer la ventilación.";
    item.precio = 40;
    item.rank = 2;
    list.push(item);
    //console.log(list[2].desc);

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 3;
    item.img = "./img/botas.jpg";
    item.alt = "Botas";
    item.desc = "Botas waterproof con membrana 'Goretex' y protecciones extra.";
    item.precio = 100;
    item.rank = 1;
    list.push(item);
    //console.log(list[3].desc);

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 4;
    item.img = "./img/guantes2.jpg";
    item.alt = "Guantes";
    item.desc = "Guantes de invierno waterproof con protecciones incluídas.";
    item.precio = 60;
    item.rank = 3;
    list.push(item);
    //console.log(list[4].desc);

    var item = { id: "", img: "", alt:"", desc: "", precio: "", rank: "" };
    item.id = 5;
    item.img = "./img/casco2.jpg";
    item.alt = "Casco";
    item.desc = "Casco modular homologado con visor solar incorporado.";
    item.precio = 180;
    item.rank = 4;
    list.push(item);
    //console.log(list[5].desc);

    return list;
}