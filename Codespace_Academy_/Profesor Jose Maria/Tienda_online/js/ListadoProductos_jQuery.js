/***********  LÓGICA DE NEGOCIO ***********/

function ObtenerProducto() {

    var lista = CargaProducto();
    var lista_elementos = $("#lista");

    $.each(lista, function (indexInArray, element) {

        /*********  ELEMENTOS *******/

        var imagen = $("<img>", {
            src: element.img,
            alt: element.alt
        }).click(function(){
            window.open("./DetalleProducto.html?detail=" + element.id, "nombrePop-Up", "width=1450, height=730, top=85,left=200");
        });

        var descripcion = $("<p>").text(element.desc);

        var precio = $("<p>").text(element.precio);

        // aquí va la creación del ranking
        var ranking = CrearRanking(element.rank);

        /*********** CONTENEDORES DE LOS ELEMENTOS ************/

        var div_imagen = $("<div>", {
            "class": "imagen"
        }).append(imagen);

        var div_descripcion = $("<div>", {
            "class": "descripcion"
        }).append(descripcion);

        var div_precio = $("<div>", {
            "class": "precio"
        }).append(precio);

        var div_ranking = $("<div>", {
            "class": "valoracion"
        }).append(ranking);
        
        var li = $("<li>", {
            "class": "col-4 details"
        });

        /*********** INSERTAR ELEMENTOS EN EL LI *************/
        $(li).append(div_imagen);
        $(li).append(div_descripcion);
        $(li).append(div_precio);
        $(li).append(div_ranking);

        $(lista_elementos).append(li);

    });



}

/*CREACIÓN RANKING DE LOS PRODUCTOS*/

function CrearRanking(rank) {
    console.log(rank);
    var ranking = $("<p>");
    for (let index = 0; index < 5; index++) {
        let checked = "";
        if (rank > index) {
            checked = " checked";
        }
        var span = $("<span>", {
            "class": " fa fa-star" + checked
        });

        $(ranking).append(span);
    }
    return ranking;

}


