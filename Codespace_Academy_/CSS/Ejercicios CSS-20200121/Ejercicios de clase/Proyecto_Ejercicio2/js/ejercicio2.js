//****************** INFO DEL SERVIDOR DE ALEJANDRO ************************

// url Servidor: 

//https://www.hiniestic.com/serverCode/servidor.php

// Parametros:
// - metodo: post | get
// - tipo: html | json | xml
// - todos los parametros que queramos

//https://www.hiniestic.com/serverCode/tabla.php

// Parametros:
// - page = page_number


//***************** CONSTANTES PARA LAS URL DEL SERVIDOR ******************

const URL_MENU = "https://www.hiniestic.com/serverCode/menu.php";
const URL_TABLA = "https://www.hiniestic.com/serverCode/tabla.php";



//******************JQUERY READY ****************

$(document).ready(function () { //Aquí empieza el ready de jQuery.


    $("#lista").hide(); //Oculta directamente el menú oculto al cargar la página (por si acaso).

    $("#hiddenMenu").on("click", function (event) { //Haciendo click le enviamos una peticion al servidor.
        event.preventDefault();
        $.ajax({
            url: URL_MENU, //Esta es la dirección a la que mandaremos la petición.
            dataType: "json", //Estos son los tipos de datos y parámetros que estamos pidiendo.
            type: "get",
            data: {
                metodo: "get",
                tipo: "json",
                apodo: "mastodonte"
            },
            success: function (respuesta) { //Cuando hay éxito en la respuesta (success) nos devolverá lo pedido anteriormente.
                //console.log("respuesta", respuesta);
                //console.log("Menu", datosMenu(respuesta));
                var lista = datosMenu(respuesta); //Utilizaremos los datos recibidos para pasarlos por la función datosMenu creada más abajo.
                $("#lista").html(lista);
            }
        }).done(function () {
            if ($("#lista").is(":visible")) { //Comprobamos si la lista está visible, para esconderla. Si no pues se mostrará para visualizarla.
                // console.log("estado", $("#lista").is(":visible"));
                $("#lista").slideUp();
            } else {
                //console.log("estado", $("#lista").is(":visible"));
                $("#lista").slideDown();
            }
        });
    });



    $(".pages > a").on("click", function (event) { //Otra petición al servidor, para obtener datos de las páginas que queremos mostrar en la lista tickets.
        event.preventDefault();
        $(".pages > a").css("background-color", "white"); //Da estilo a los botones de selección de página cuando clickamos.
        $(".pages > a").css("color", "rgb(4, 163, 226)");
        $(this).css("background-color", "rgb(4, 163, 226)");
        $(this).css("color", "white");
        $.ajax({
            url: URL_TABLA,
            data: {
                page: getPage($(this).text()) //Desarrollado en la función getPage, en el apartado FUNCIONES.
            },
            success: function (response) {
                //console.log("Page", response);
                pageData(response);
            }
        })
    });



}); //Aquí termina el ready de jQuery.




//****************************  FUNCIONES PARA PROCESAR LA PETICIÓN DEL SERVIDOR ********************************


// Función que comprueba el valor input al clickar un número, previous o next, y va restando o sumando una página. 
var pagina = 0;

function getPage(input) {
    if (isNaN(Number(input))) {
        if (input === "Previous") {
            if (pagina > 0) {
                pagina = pagina - 1;
            }
        } else {
            pagina = pagina + 1;
        }
    } else {
        pagina = input;
    }
    return pagina;
}


// Función para recorrer el array page e introducir los li nuevos en la lista con los datos actualizados.
function pageData(data) {
    $(".columns li").remove();
    var classDiv = ["col-1", "col-5", "col-3", "col-3"];
    var rowList = "";
    for (var i = 0; i < data.tickets.length; i++) {
        rowList = '<li class="rowtable table_columns">'; //Aquí creamos la variable de la fila, y la abrimos con el li.
        for (var j = 0; j < data.tickets[i].length; j++) {
            rowList = rowList + '<div class="' + classDiv[j] + '">' + '<p>' + data.tickets[i][j] + '</p>' + '</div>'; //Aquí le añadimos el resto de la estructura (con los datos) que contiene el li.
        }
        rowList = rowList + '</li>'; //Aquí cerramos el li creado en la primera variable rowList.
        $(".columns").append(rowList);
    }
}


// Función para recorrer el array JSON e introducir los li en la lista creada.

function datosMenu(data) {
    var iconList = ['<ion-icon name="power"></ion-icon>', '<ion-icon name="settings"></ion-icon>', '<ion-icon name="share"></ion-icon>', '<ion-icon name="color-wand"></ion-icon>'];
    var elementoLista = "";
    for (var i = 0; i < data.itemsMenu.length; i++) {
        elementoLista += "<li>" + iconList[i] + data.itemsMenu[i] + "</li>";
    }
    return elementoLista;
}





// ********************* FUNCIÓN DE FUNCIONES PARA ORDENAR LOS DATOS DE LA LISTA ****************************


// Funcion para ordenar la lista por Id y pintarla de nuevo en la tabla.

function orderById() {
    var listaElementos = [];
    $(".columns li").each(function (index1, element1) {  //De esta forma obtenemos los arrays que ordenaremos luego. Obtendremos un array de arrays.
        listaElementos[index1] = [];
        $(element1).find("div").each(function (index2, element2) {
            listaElementos[index1][index2] = $(element2).find("p").text();
            //console.log("listaElementos", listaElementos);
        })
    });

    function sort(a, b) { //Función para ordenar el array obtenido.
        //console.log(a);
        //console.log(b);
        a = a[0];
        b = b[0];
        if (a < b) {
            return -1;
        } else if (a > b) {
            return 1;
        }
        return 0;
    }
    var orderedList = listaElementos.sort(sort);    //Ya tenemos la lista ordenada.
    //console.log("Lista Ordenada", orderedList);

    $(".columns li").remove(); //Borramos los li de la lista para pintarlos de nuevo.

    var classDiv = ["col-1", "col-5", "col-3", "col-3"]; //Creamos esta variable para ir añadiéndole una clase determinada a cada div que se crea dentro del li.

    //Hacemos un doble for para recorrer la lista ordenada y poder así crear los nuevos li.
    for (var i = 0; i < orderedList.length; i++) {

        var rowList = '<li class="rowtable table_columns">'; //Aquí creamos la variable de la fila, y la abrimos con el li.

        for (var j = 0; j < orderedList[i].length; j++) {
            rowList = rowList + '<div class="' + classDiv[j] + '">' + '<p>' + orderedList[i][j] + '</p>' + '</div>'; //Aquí le añadimos el resto de la estructura (con los datos) que contiene el li.
        }
        rowList = rowList + '</li>'; //Aquí cerramos el li creado en la primera variable rowList.

        $(".columns").append(rowList); //Añadimos los nuevos li a la lista.
    };




    //Esta es la forma bruta de hacerlo, metiendo la estructura en la variable a fuego con los valores.

    // var finalList = '<li class="rowtable table_columns">' 
    // + '<div class="col-1"> '
    // + '<p>'+ orderedList[0][0] +'</p>'
    // + '</div>'
    // + '<div class="col-5">'
    // + '<p>'+ orderedList[0][1] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[0][2] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[0][3] +'</p>'
    // + '</div>'
    // + '</li>'
    // + '<li class="table_columns">'
    // + '<div class="col-1"> '
    // + '<p>'+ orderedList[1][0] +'</p>'
    // + '</div>'
    // + '<div class="col-5">'
    // + '<p>'+ orderedList[1][1] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[1][2] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[1][3] +'</p>'
    // + '</div>'
    // + '</li>'
    // + '<li class="rowtable table_columns">'
    // + '<div class="col-1"> '
    // + '<p>'+ orderedList[2][0] +'</p>'
    // + '</div>'
    // + '<div class="col-5">'
    // + '<p>'+ orderedList[2][1] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[2][2] +'</p>'
    // + '</div>'
    // + '<div class="col-3">'
    // + '<p>'+ orderedList[2][3] +'</p>'
    // + '</div>'
    // + '</li>';

    //$(".columns").append(finalList);
};
