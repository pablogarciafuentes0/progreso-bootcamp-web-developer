$(document).ready(function () {

    $("#header").css('background', 'lightgrey').css('padding', 20).css('border', '2px solid black');
    $("#menu").css('background', 'lightblue').css('padding', 30);
    $("ul > li").css('background', 'white').css('padding', 5).css('border', '2px solid green');
    $('#title').css('padding', 60).css('background', 'green');
    $('#banner').css('padding', 60).css('background', 'purple').css("color", "white");



    $('#header').click(function (e) {
        e.preventDefault();
        $("#content").css("background", "red").css("color", "white").css("padding", 10);
    });

    $("#content").click(function (e) {
        e.preventDefault();
        $("#menu").fadeOut(2500);
    });

    $("p").click(function (e) {
        e.preventDefault();
        $("#menu").fadeIn(2500);
    });



    $("#menu, #title, #banner").css("max-height", 200).css("min-height", 200);


    $("#menu").click(function (e) {
        e.preventDefault();
        $("#banner").slideToggle(2000);
    });






});

