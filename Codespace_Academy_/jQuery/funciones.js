$(document).ready(function () {

  var elementoMenu = $("input[name*='n_input']");
  console.log(elementoMenu);

});

/*Obtener Subpagina2.2 de diferentes formas.*/

$(document).ready(function () {
  var elementoSubpagina22 = $("ul li:eq(6)");
  console.log(elementoSubpagina22);

  var elementoSubpagina22 = $("ul>li>ul>li:contains('2.2')");
  console.log(elementoSubpagina22);

  var elementoSubpagina22 = $(".submenu li:contains('2.2')");
  console.log(elementoSubpagina22);

  var elementoSubpagina22 = $("li:nth-child(2) li:nth-child(2)");
  console.log(elementoSubpagina22);

  var elementoSubpagina22 = $("#menu .submenu:nth-child(2) li:contains('2.2')");
  console.log(elementoSubpagina22);

  var elementoSubpagina22 = $("#menu li:nth-child(2) > ul > li:nth-child(2)");
  console.log(elementoSubpagina22);
});

/*Consultar si #menu contiene la clase 'submenu'*/
console.log($('#menu').hasClass('submenu'));



$(document).ready(function () {

  /*Concatenar propiedades*/
  var prop = $('#input1');
  prop.val("Nuevo valor").css('background', 'black').css('color', 'white').css('border', '20px solid red').append('<label>El label</label>');
  console.log(prop);
  var prop2 = $('#input2');
  prop2.css('color', 'white').css('background', 'green').css('border', '5px solid blue');
  console.log(prop2);

  /*Funcion de prueba para ver cómo funciona el evento 'on'*/
  /*function greet(event) {
    alert("Hello " + event.data.name);
  }
  $("button").on("click", {
    name: "Karl"
  }, greet);
  $("button").on("click", {
    name: "Addy"
  }, greet);
  */

  /*Funcion para ver cómo funciona el trigger, ejecuta otro evento creado en otro elemento*/
  $("#textoquelanza").click(function () {
    $("button").trigger("click");
  });

  $("button").on("click", function () {
    if ($("#textoquelanza").is(":visible")) {
      $("#textoquelanza").hide();
    } else {
      $("#textoquelanza").show();
    }
  });


  /*El código de abajo ocultará los submenus al cargar la página, si hacemos click
  en Pagina 1, Pagina 2 o Página 3 nos mostrará el submenú que contiene, y si volvemos
  a clickar nos lo ocultará. También si pasamos el ratón por encima de Página 3, nos 
  mostrará el submenú mientras el ratón esté encima de éste, si lo sacamos fuera se 
  volverá a ocultar*/

  $(".submenu > ul").hide();
  /*
    $(".submenu:nth-child(1)").click(function (e) {
      e.preventDefault();
      if ($(".submenu:nth-child(1) > ul").is(":visible")) {
        $(".submenu:nth-child(1) > ul").hide();
      } else {
        $(".submenu:nth-child(1) > ul").show();
        $(".submenu:nth-child(2) > ul").hide();
        $(".submenu:nth-child(3) > ul").hide();
      }
    });
  
    $(".submenu:nth-child(2)").click(function (e) {
      e.preventDefault();
      if ($(".submenu:nth-child(2) > ul").is(":visible")) {
        $(".submenu:nth-child(2) > ul").hide();
      } else {
        $(".submenu:nth-child(2) > ul").show();
        $(".submenu:nth-child(1) > ul").hide();
        $(".submenu:nth-child(3) > ul").hide();
      }
    });
  
    $(".submenu:nth-child(3)").click(function (e) {
      e.preventDefault();
      if ($(".submenu:nth-child(3) > ul").is(":visible")) {
        $(".submenu:nth-child(3) > ul").hide();
      } else {
        $(".submenu:nth-child(3) > ul").show();
        $(".submenu:nth-child(2) > ul").hide();
        $(".submenu:nth-child(1) > ul").hide();
      }
    });
  */

  /*Esta es la solución para el hover */

  $(".submenu").css("padding", 20);

  $(".submenu:nth-child(3)").hover(function () {
    if ($(".submenu:nth-child(3) > ul").is(":visible")) {
      $(".submenu:nth-child(3) > ul").hide();
    } else {
      $(".submenu:nth-child(3) > ul").show();
      $(".submenu:nth-child(2) > ul").hide();
      $(".submenu:nth-child(1) > ul").hide();
    }
  });

  $(".submenu:nth-child(3)").focusin(function () {
    $(".submenu:nth-child(3) > ul").show();
  });

  $(".submenu:nth-child(3)").focusout(function () {
    $(".submenu:nth-child(3) > ul").hide();

  });


  /*Esta es la solución de Alejandro para el ejercicio anterior.*/
  /*
  $(".submenu").on("click", function(){
    if ($(this).find("ul").is(":visible")){
      $(".submenu > ul").hide();
    }else{
      $(".submenu > ul").hide();
      $(this).find("ul").show();
    }
  });
*/

/*Esta es la solución si sacamos los submenús a otro div distinto, hemos comentado
los ul del ejercicio anterior.*/
  $("#submenus > ul").hide();

  var indice=0;

  $(".submenu").on("click", function () {
    indice=($(this).index());
    console.log(indice);

    if ($("#submenus ul:eq("+ indice +")").is(":visible")) {
      $("#submenus ul:eq("+ indice +")").hide();
    } else {
      $("#submenus > ul").hide();
      $("#submenus ul:eq("+ indice +")").show();
    }
  });










});